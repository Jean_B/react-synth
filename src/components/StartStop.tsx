import { useEffect, useState } from "react";

interface props {
		gainNode: GainNode;
		destination: AudioNode;
		onClick: () => void;
}

const StartStop = ({ gainNode, destination, onClick }: props) => {

		const onStartClick = () => {
				gainNode.connect(destination);
				onClick();
		}

		const onStopClick = () => {
				gainNode.disconnect(destination);
		}

		return (
				<div>
						<div onClick={onStartClick}>Start</div>
						<div onClick={onStopClick}>Stop</div>
				</div>
		);
};

export default StartStop;
