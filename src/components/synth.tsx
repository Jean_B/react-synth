import { useState } from 'react';
import GainControl from './GainControl';
import OscillatorControl from './OscillatorControl';
import StartStop from './StartStop';
import '../App.scss';

interface props {
		contexteAudio: AudioContext;
		mainGainNode: GainNode;
}

interface SynthNode {
		gainNode: GainNode;
		oscillatorNode: OscillatorNode;
}

const Synth = ({ contexteAudio, mainGainNode }: props) => {
		const [synthNodes, setSynthNodes] = useState<SynthNode[]>([]);

		const addNode = () => {
				const oscillatorNode = contexteAudio.createOscillator();
				oscillatorNode.type = 'sine';
				oscillatorNode.frequency.value = 440;
				oscillatorNode.start();

				const gainNode = contexteAudio.createGain();
				
				gainNode.gain.value = 0.1;
				oscillatorNode.connect(gainNode);

				const newNode = {
						oscillatorNode,
						gainNode
				}

				setSynthNodes([...synthNodes, newNode]);
		};
		
		return (
				<div>
						<button onClick={addNode}>Add Node</button>
						<div>
						{
								synthNodes.map(({ gainNode, oscillatorNode }: SynthNode, index) => (
										<div className="nodes-container">
												<OscillatorControl oscillator={oscillatorNode} name={`osc_${index}`} />
												<GainControl gainNode={gainNode} />
												<StartStop gainNode={gainNode} destination={mainGainNode} onClick={() => {contexteAudio.resume()}}/>
										</div>
								))
						}
						</div>
				</div>
		);
};

export default Synth;
