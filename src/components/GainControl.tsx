import { useEffect, useState } from "react";

interface props {
		gainNode: GainNode
}

const gainDataToVue = (value: number) => Math.floor(value * 100)

const GainControl = ({ gainNode }: props) => {

		const [gain, setGain] = useState<number>(gainDataToVue(gainNode.gain.value));

		const onVolumeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
				setGain(parseInt(e.target.value, 10));
		};

		useEffect(() => {
				gainNode.gain.value = gain / 100;
		}, [gain, gainNode.gain]);

		useEffect(() => {
				setGain(gainDataToVue(gainNode.gain.value));
		}, [gainNode.gain]);

		return (
				<div>
						<div>Volume: {gain}</div>
						<input type="range" id="volume" name="volume" min="0" max="100" onChange={onVolumeChange} value={gain} />
				</div>
		);
};

export default GainControl;
