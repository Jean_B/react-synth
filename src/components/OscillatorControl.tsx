import { useEffect, useState } from "react";

interface props {
		oscillator: OscillatorNode;
		name: string;
}


const getFrequencyByNote = (note: number) => {
		return 440 * Math.pow(2, (note - 69) / 12);
}

const OscillatorControl = ({ oscillator, name }: props) => {

		const [type, setType] = useState<OscillatorType>(oscillator.type);
		const [midiNote, setmidiNote] = useState<number>(69);

		const changeValue = (e: any) => {
				const value = e?.target?.value;
				if(value) {
						setType(value);
				}
		}

		useEffect(() => {
				oscillator.frequency.value = getFrequencyByNote(midiNote);
		}, [oscillator, midiNote]);


		useEffect(() => {
				oscillator.type = type;
		}, [oscillator, type]);

		useEffect(() => {
				setType(oscillator.type);
		}, [oscillator]);

		return (
			<div>
					<div>
							<div>
									<input type="radio" id={`sine_${name}`} name={name} value="sine" checked={type === "sine"} onChange={changeValue}/>
									<label htmlFor={`sine_${name}`}>sine</label>
							</div>
							<div>
									<input type="radio" id={`triangle_${name}`} name={name} value="triangle" checked={type === "triangle"} onChange={changeValue}/>
									<label htmlFor={`triangle_${name}`}>triangle</label>
							</div>	
							<div>
									<input type="radio" id={`square_${name}`} name={name} value="square" checked={type === "square"} onChange={changeValue}/>
									<label htmlFor={`square_${name}`}>square</label>
							</div>
							<div>
									<input type="radio" id={`sawtooth_${name}`} name={name} value="sawtooth" checked={type === "sawtooth"} onChange={changeValue}/>
									<label htmlFor={`sawtooth_${name}`}>sawtooth</label>
							</div>
					</div>
					<div>
							<div>Pitch: {midiNote}</div>
							<input type="range" id="pitch" name={`pitch_${name}`} min="2" max="127" onChange={(e) => setmidiNote(parseInt(e.target.value, 10))} value={midiNote} />
					</div>
			</div>	
		);
};

export default OscillatorControl;
