import './App.scss';
import Synth from './components/synth';
import GainControl from './components/GainControl';

function App() {
		const contexteAudio = new window.AudioContext();
		const gainNode = contexteAudio.createGain();
		gainNode.gain.value = 0.1;
		gainNode.connect(contexteAudio.destination);
		return (
				<div className="App">
						<GainControl gainNode={gainNode} />
						<Synth contexteAudio={contexteAudio} mainGainNode={gainNode} />
				</div>
		);
}

export default App;
